<?php

namespace Raf\Itadakimasu\Config;

class Config {

    const TEMPLATE_DIR = __DIR__ . '/../View/templates';
    
    const DB_CONNECTION_DATA=[
    'dbname' => 'itadakimasu',
    'user' => 'raf',
    'password' => 'qwerty',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
    'charset' => 'utf8'
    ];
    
}