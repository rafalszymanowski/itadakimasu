<?php

namespace Raf\Itadakimasu\Controller;

use Doctrine\DBAL\Connection;
use Raf\Itadakimasu\Recipe\Recipe;
use Raf\Itadakimasu\Recipe\RecipeComment;
use Raf\Itadakimasu\Product\Product;
use Raf\Itadakimasu\Response\Response;
use Raf\Itadakimasu\Review\Review;
use Raf\Itadakimasu\Review\ReviewComment;
use Raf\Itadakimasu\View\Template;
use Raf\Itadakimasu\Request\Request;
use Raf\Itadakimasu\Session\Session;
use Raf\Itadakimasu\User\User;
use Raf\Itadakimasu\User\UserRepository;
use Raf\Itadakimasu\User\RegisteredUser;
use Raf\Itadakimasu\User\RegisteredUserRepository;
use Raf\Itadakimasu\Recipe\RecipeRepository;
use Raf\Itadakimasu\Product\ProductRepository;
use Raf\Itadakimasu\Review\ReviewRepository;
use Raf\Itadakimasu\Recipe\RecipeCommentRepository;
use Raf\Itadakimasu\Review\ReviewCommentRepository;

class Itadakimasu {

    /**
     * @var Template
     */
    private $templateEngine;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var RecipeRepository
     */
    private $recipeRepo;

    /**
     * @var ProductRepository
     */
    private $productRepo;

    /**
     * @var ReviewRepository
     */
    private $reviewRepo;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var
     */
    private $regUserRepo;

    /**
     * @var RecipeCommentRepository
     */
    private $recipeCommentRepo;

    /**
     * @var ReviewCommentRepository
     */
    private $reviewCommentRepo;

    /**
     * @var Connection
     */
    private $dbh;

    /**
     * @var array
     */
    private $templateParameters = [];

    /**
     * @var int
     */
    private $page = 1;

    /**
     * Itadakimasu constructor.
     * @param Template $templateEngine
     * @param Request $request
     * @param Session $session
     * @param RecipeRepository $recipeRepo
     * @param ProductRepository $productRepo
     * @param ReviewRepository $reviewRepo
     * @param UserRepository $userRepository
     * @param RegisteredUserRepository $regUserRepo
     * @param RecipeCommentRepository $recipeCommentRepo
     * @param ReviewCommentRepository $reviewCommentRepo
     * @param Connection $dbh
     */
    public function __construct(
        Template $templateEngine,
        Request $request,
        Session $session,
        RecipeRepository $recipeRepo,
        ProductRepository $productRepo,
        ReviewRepository $reviewRepo,
        UserRepository $userRepository,
        RegisteredUserRepository $regUserRepo,
        RecipeCommentRepository $recipeCommentRepo,
        ReviewCommentRepository $reviewCommentRepo,
        Connection $dbh
    ) {
        $this->templateEngine = $templateEngine;
        $this->request = $request;
        $this->session = $session;
        $this->recipeRepo = $recipeRepo;
        $this->productRepo = $productRepo;
        $this->reviewRepo = $reviewRepo;
        $this->userRepo = $userRepository;
        $this->regUserRepo = $regUserRepo;
        $this->recipeCommentRepo = $recipeCommentRepo;
        $this->reviewCommentRepo = $reviewCommentRepo;
        $this->dbh = $dbh;
    }

    public function run()
    {
        $action = array_key_exists('action', $_GET) ? $_GET['action'] : $action = 'homepage';
        $username = $this->session->getFromSession('username');
        
        if ('' === $username){
            $username = $this->request->getParamFromPost('username', '');
        }
        
        if ('' === $username && 'account' === $action) {
            header('Location: index.php?action=homepage');
        }

        $this->templateParameters['username'] = $username;
        $this->templateParameters['action'] = $action;

        $templateName = '';

        switch ($action) {
            case 'homepage':
                $templateName = 'homepage.html';
                $newestRecipe = $this->recipeRepo->getNewestRecipe();
                $this->templateParameters['homepage'] = $newestRecipe;
                break;
            case 'about_me':
                $templateName = 'about_me.html';
                $adminInfo = $this->regUserRepo->getInfoAboutAdmin();
                $this->templateParameters['about_me'] = $adminInfo;
                break;
            case 'recipes_menu':
                $templateName = 'recipes_menu.html';
                $recipeCategory = $this->request->getParamFromGet('recipe_cat');
                $recipeList = $this->recipeRepo->getRecipesByCategory($recipeCategory);
                $this->templateParameters['recipe_list'] = $recipeList;
                break;
            case 'recipe':
                $templateName = 'recipe.html';
                $recipeId = $this->request->getParamFromGet('recipe_id');
                $singleRecipe = $this->recipeRepo->getRecipe($recipeId);
                $recipeComment = $this->recipeCommentRepo->getRecipeComments($recipeId);
                $this->templateParameters['comments'] = $recipeComment;
                $this->templateParameters['recipe'] = $singleRecipe;
                break;
            case 'add_recipe_comment':
                $templateName = 'recipe.html';
                $recipeId = $this->request->getParamFromPost('recipe_id');
                $comment = $this->request->getParamFromPost('comment');
                $username = $this->session->getFromSession('username');
                $userId = $this->regUserRepo->getUserId($username);
                $singleRecipe = $this->recipeRepo->getRecipe($recipeId);
                $this->recipeCommentRepo->saveRecipeComment($comment, $recipeId, $userId);
                $recipeComment = $this->recipeCommentRepo->getRecipeComments($recipeId);
                $this->templateParameters['comments'] = $recipeComment;
                $this->templateParameters['recipe'] = $singleRecipe;
                header('Location: index.php?action=recipe&recipe_id=' . $recipeId);
                break;
            case 'delete_recipe_comment':
                $templateName = 'recipe.html';
                $recipeId = $this->request->getParamFromGet('recipe_id');
                $commentId = $this->request->getParamFromGet('comment_id');
                $recipeComment = new RecipeComment(
                    $commentId,
                    null,
                    null,
                    null
                );
                $this->recipeCommentRepo->deleteRecipeComment($recipeComment);
                header('Location: index.php?action=recipe&recipe_id=' . $recipeId);
                break;
            case 'add_recipe':
                $templateName = 'add_recipe.html';
                break;
            case 'save_recipe':
                $recipe = new Recipe(
                    null,
                    $_POST['recipe_name'],
                    $_POST['recipe_main_photo'],
                    $_POST['recipe_category'],
                    $_POST['editor']
                );
                $this->recipeRepo->saveRecipe($recipe);
                header('Location: index.php?action=account');
                break;
            case 'edit_recipe':
                $templateName = 'edit_recipe.html';
                if(isset($_POST['recipe_edit'])) {
                    $recipe = new Recipe(
                        $_POST['recipe_id'],
                        $_POST['recipe_name'],
                        $_POST['recipe_main_photo'],
                        $_POST['recipe_category'],
                        $_POST['recipe_edit']
                    );
                    $this->recipeRepo->updateRecipe($recipe);
                }
                $recipeId = $this->request->getParamFromGet('recipe_id');
                $singleRecipe = $this->recipeRepo->getRecipe($recipeId);
                $this->templateParameters['recipe'] = $singleRecipe;
                break;
            case 'delete_recipe':
                $recipe = new Recipe(
                    $_GET['recipe_id'],
                    null,
                    null,
                    null
                );
                $this->recipeRepo->deleteRecipe($recipe);
                header('Location: index.php?action=account');
                break;
            case 'search_product':
                $templateName = 'products_menu.html';
                $productName = $this->request->getParamFromPost('search');
                $searchedProduct = $this->productRepo->searchForSingleProduct($productName);
                $this->templateParameters['products_menu'] = $searchedProduct;
                break;
            case 'products_menu':
                $templateName = 'products_menu.html';
                $actualPage = $this->request->getParamFromGet('page', 1);
                $this->templateParameters['current_page'] = $actualPage;
                $pagesAmount = $this->productRepo->getAmountOfPages();
                $this->templateParameters['limit_to'] = $pagesAmount;
                if($this->request->getParamFromGet('page') != '') {
                    $this->page = $this->request->getParamFromGet('page');
                }
                $productsOnListAmount = $this->productRepo->getLimitedAmountOfProductsOnList($this->page);
                $this->templateParameters['products_menu'] = $productsOnListAmount;
                break;
            case 'product':
                $templateName = 'product.html';
                $productId = $this->request->getParamFromGet('product_id');
                $singleProduct = $this->productRepo->getProduct($productId);
                $this->templateParameters['product'] = $singleProduct;
                break;
            case 'add_product':
                $templateName = 'add_product.html';
                break;
            case 'save_product':
                $product = new Product(
                    null,
                    $_POST['product_name'],
                    $_POST['editor']
                );
                $this->productRepo->saveProduct($product);
                header('Location: index.php?action=account');
                break;
            case 'edit_product':
                $templateName = 'edit_product.html';
                if (isset($_POST['product_edit'])) {
                    $product = new Product(
                        $_POST['product_id'],
                        $_POST['product_name'],
                        $_POST['product_edit']
                    );
                    $this->productRepo->updateProduct($product);
                }
                $productId = $this->request->getParamFromGet('product_id');
                $singleProduct = $this->productRepo->getProduct($productId);
                $this->templateParameters['product'] = $singleProduct;
                break;
            case 'delete_product':
                $product = new Product(
                    $_GET['product_id'],
                    null,
                    null,
                    null
                );
                $this->productRepo->deleteProduct($product);
                header('Location: index.php?action=account');
                break;
            case 'reviews_menu':
                $templateName = 'reviews_menu.html';
                $reviewCity = $this->request->getParamFromGet('review_city');
                $reviewList = $this->reviewRepo->getReviewsByCity($reviewCity);
                $this->templateParameters['review_list'] = $reviewList;
                break;
            case 'review':
                $templateName = 'review.html';
                $reviewId = $this->request->getParamFromGet('review_id');
                $singleReview = $this->reviewRepo->getReview($reviewId);
                $reviewComment = $this->reviewCommentRepo->getReviewComments($reviewId);
                $this->templateParameters['comments'] = $reviewComment;
                $this->templateParameters['review'] = $singleReview;
                break;
            case 'add_review_comment':
                $templateName = 'review.html';
                $reviewId = $this->request->getParamFromPost('review_id');
                $comment = $this->request->getParamFromPost('comment');
                $username = $this->session->getFromSession('username');
                $userId = $this->regUserRepo->getUserId($username);
                $singleReview = $this->reviewRepo->getReview($reviewId);
                $this->reviewCommentRepo->saveReviewComment($comment, $reviewId, $userId);
                $reviewComment = $this->reviewCommentRepo->getReviewComments($reviewId);
                $this->templateParameters['comments'] = $reviewComment;
                $this->templateParameters['review'] = $singleReview;
                header('Location: index.php?action=review&review_id=' . $reviewId);
                break;
            case 'delete_review_comment':
                $templateName = 'review.html';
                $reviewId = $this->request->getParamFromGet('review_id');
                $commentId = $this->request->getParamFromGet('comment_id');
                $reviewComment = new ReviewComment(
                    $commentId,
                    null,
                    null,
                    null
                );
                $this->reviewCommentRepo->deleteReviewComment($reviewComment);
                header('Location: index.php?action=review&review_id=' . $reviewId);
                break;
            case 'add_review':
                $templateName = 'add_review.html';
                break;
            case 'save_review':
                $review = new Review(
                    null,
                    $_POST['review_name'],
                    $_POST['review_city'],
                    $_POST['review_main_photo'],
                    $_POST['editor']
            );
                $this->reviewRepo->saveReview($review);
                header('Location: index.php?action=account');
                break;
            case 'edit_review':
                $templateName = 'edit_review.html';
                if (isset($_POST['review_edit'])) {
                    $review = new Review(
                        $_POST['review_id'],
                        $_POST['review_name'],
                        $_POST['review_city'],
                        $_POST['review_main_photo'],
                        $_POST['review_edit']
                    );
                    $this->reviewRepo->updateReview($review);
                }
                $reviewId = $this->request->getParamFromGet('review_id');
                $singleReview = $this->reviewRepo->getReview($reviewId);
                $this->templateParameters['review'] = $singleReview;
                break;
            case 'delete_review':
                $review = new Review(
                    $_GET['review_id'],
                    null,
                    null,
                    null
                );
                $this->reviewRepo->deleteReview($review);
                header('Location: index.php?action=account');
                break;
            case 'contact':
                $templateName = 'contact.html';
                break;
            case 'login':
                $this->login();
                break;
            case 'register':
                $this->register();
                break;
            case 'account':
                $templateName = 'account.html';
                $this->showStatisticsForAdmin();
                break;
            case 'logout':
                $this->session->sessionDestroy();
                header('Location: index.php?action=homepage');
                break;
            default:
                $templateName = 'error404.html';
                break;
        }
        
        $this->templateEngine->renderTemplate($templateName, $this->templateParameters);
    }

    private function login()
    {
        if($_POST['username'] != '' && $_POST['password'] != '') {
            $user = new User(
                htmlspecialchars($_POST['username']),
                htmlspecialchars($_POST['password'])
            );
            if($this->userRepo->validateUser($user) === false) {
                Response::send('', ['Niepoprawny login lub hasło!']);
            } else {
                $this->session->saveToSession('username', $_POST['username']);
                Response::send('ok');
            }
        };
        Response::send('', ['Podaj login i hasło!']);
    }
    
    private function register()
    {
        if($_POST['username'] != '' &&
            $_POST['user_email'] != '' &&
            $_POST['password'] != '')
        {
            $registeredUser = new RegisteredUser(
                null,
                htmlspecialchars($_POST['username']),
                htmlspecialchars($_POST['user_email']),
                htmlspecialchars($_POST['password'])
            );
            
            if($this->regUserRepo->loginAlreadyExists($registeredUser) === true) {
                Response::send('', ['Użytkownik o podanym loginie istnieje już w bazie!']);
            } elseif($this->regUserRepo->emailAlreadyExists($registeredUser) === true) {
                Response::send('', ['Podany adres e-mail istnieje już w bazie!']);
            } elseif($this->regUserRepo->addNewUser($registeredUser) === true) {
                $this->session->saveToSession('username', $registeredUser->getUsername());
                Response::send('ok');
            } else {
                Response::send('', ['Podany adres e-mail nie jest poprawny!']);
            }
        }
        Response::send('', ['Wypełnij wszystkie pola danymi!']);
    }

    private function showStatisticsForAdmin()
    {
        $usersAmount = $this->regUserRepo->getAmountOfRegisteredUsers();
        $this->templateParameters['usersAmount'] = $usersAmount;
        $productsAmount = $this->productRepo->getAmountOfProducts();
        $this->templateParameters['productsAmount'] = $productsAmount;
        $recipesAmount = $this->recipeRepo->getAmountOfRecipes();
        $this->templateParameters['recipesAmount'] = $recipesAmount;
        $reviewsAmount = $this->reviewRepo->getAmountOfReviews();
        $this->templateParameters['reviewsAmount'] = $reviewsAmount;
        $recipeCommentsAmount = $this->recipeCommentRepo->getAmountOfCommentsForRecipes();
        $this->templateParameters['recipeCommentsAmount'] = $recipeCommentsAmount;
        $reviewCommentsAmount = $this->reviewCommentRepo->getAmountOfCommentsForReviews();
        $this->templateParameters['reviewCommentsAmount'] = $reviewCommentsAmount;
    }

}