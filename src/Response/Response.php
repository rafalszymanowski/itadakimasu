<?php

namespace Raf\Itadakimasu\Response;

class Response {
    
    const STATUS_OK = 200;
    const STATUS_BAD_REQUEST = 400;
    const STATUS_PERMISSION_DENIED = 403;
    const STATUS_NOT_FOUND = 404;

    public static function send($dataToSend, $errors = [], $statusCode = self::STATUS_OK){
        
        http_response_code($statusCode);

        $data = [
            'errors' => $errors,
            'data' => $dataToSend
        ];
        
        echo json_encode($data);
        exit();
    }
}
