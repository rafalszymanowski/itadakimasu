<?php

namespace Raf\Itadakimasu\Recipe;

use Doctrine\DBAL\Connection;

class RecipeRepository
{
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * RecipeRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getRecipe($id)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `recipes` WHERE `recipe_id` = :id');
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $recipeContent = $sth->fetch();

        return $recipeContent;
    }

    /**
     * @return array
     */
    public function getNewestRecipe()
    {
        $sth = $this->dbh->prepare('SELECT * FROM `recipes` ORDER BY `recipe_add_date` DESC LIMIT 3');
        $sth->execute();
        $newestRecipes = $sth->fetchAll();

        return $newestRecipes;
    }

    /**
     * @param $category
     * @return array
     */
    public function getRecipesByCategory($category)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `recipes` WHERE `recipe_category` = :recipe_category ORDER BY `recipe_add_date` DESC');
        $sth->bindValue('recipe_category', $category, \PDO::PARAM_STR);
        $sth->execute();
        $recipesCat = $sth->fetchAll();

        return $recipesCat;
    }

    /**
     * @return bool|string
     */
    public function getAmountOfRecipes()
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `recipes`');
        $sth->execute();
        $recipesAmount = $sth->fetchColumn();

        return $recipesAmount;
    }

    /**
     * @param Recipe $recipe
     * @return int
     */
    public function saveRecipe(Recipe $recipe)
    {
        return $this->dbh->insert('recipes', [
                'recipe_name' => $recipe->getName(),
                'recipe_main_photo' => $recipe->getMainPhoto(),
                'recipe_category' => $recipe->getCategory(),
                'recipe_content' => $recipe->getContent(),
                ]);
    }

    /**
     * @param Recipe $recipe
     * @return int
     */
    public function updateRecipe(Recipe $recipe)
    {
        return $this->dbh->update('recipes', array(
            'recipe_name' => $recipe->getName(),
            'recipe_main_photo' => $recipe->getMainPhoto(),
            'recipe_category' => $recipe->getCategory(),
            'recipe_content' => $recipe->getContent()
        ), array(
            'recipe_id' => $recipe->getId()
        ));
    }

    /**
     * @param Recipe $recipe
     * @return int
     */
    public function deleteRecipe(Recipe $recipe)
    {
        return $this->dbh->delete('recipes', array(
            'recipe_id' => $recipe->getId()
        ));
    }

}