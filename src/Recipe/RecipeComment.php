<?php
/**
 * Created by PhpStorm.
 * User: Raf
 * Date: 09.05.2017
 * Time: 19:11
 */

namespace Raf\Itadakimasu\Recipe;

class RecipeComment
{
    /**
     * @var
     */
    private $commentId;

    /**
     * @var
     */
    private $recipeId;

    /**
     * @var
     */
    private $userId;

    /**
     * @var string
     */
    private $comment;


    /**
     * RecipeComment constructor.
     * @param $commentId
     * @param $recipeId
     * @param $userId
     * @param $comment
     */
    public function __construct($commentId, $recipeId, $userId, $comment)
    {
        $this->commentId = $commentId;
        $this->recipeId = $recipeId;
        $this->userId = $userId;
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getCommentId()
    {
        return $this->commentId;
    }

    /**
     * @return mixed
     */
    public function getRecipeId()
    {
        return $this->recipeId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
}