<?php
/**
 * Created by PhpStorm.
 * User: Raf
 * Date: 09.05.2017
 * Time: 19:11
 */

namespace Raf\Itadakimasu\Recipe;

use Doctrine\DBAL\Connection;

class RecipeCommentRepository
{
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * RecipeCommentRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param $id
     * @return array
     */
    public function getRecipeComments($id)
    {
        $sth = $this->dbh->prepare(
            'SELECT
              rc.com_id,
              rc.comment,
              rc.creation_date,
              u.username
            FROM 
              `recipes_comments` AS rc 
            JOIN `recipes` AS r ON r.recipe_id = rc.recipe_id
            JOIN `users` AS u ON rc.user_id = u.user_id
            WHERE 
              r.`recipe_id` = :id ORDER BY `creation_date` DESC'
        );
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $recipeComment = $sth->fetchAll();

        return $recipeComment;
    }

    /**
     * @return bool|string
     */
    public function getAmountOfCommentsForRecipes()
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `recipes_comments`');
        $sth->execute();
        $recipeCommentsAmount = $sth->fetchColumn();

        return $recipeCommentsAmount;
    }

    /**
     * @param $comment
     * @param $recipeId
     * @param $userId
     * @return int
     */
    public function saveRecipeComment($comment, $recipeId, $userId)
    {
        if(strlen($comment) > 1) {
            return $this->dbh->insert('recipes_comments', [
                'recipe_id' => $recipeId,
                'user_id' => $userId,
                'comment' => $comment
            ]);
        }
        return false;
    }

    /**
     * @param RecipeComment $recipeComment
     * @return int
     */
    public function deleteRecipeComment(RecipeComment $recipeComment)
    {
        return $this->dbh->delete('recipes_comments', array(
            'com_id' => $recipeComment->getCommentId()
        ));
    }

}