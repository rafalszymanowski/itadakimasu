<?php
/**
 * Created by PhpStorm.
 * UserRepository: Raf
 * Date: 26.04.2017
 * Time: 20:23
 */

namespace Raf\Itadakimasu\Recipe;

class Recipe
{
    /**
     * @var
     */
    private $id;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $mainPhoto;
    /**
     * @var
     */
    private $category;
    /**
     * @var string
     */
    private $content;

    /**
     * Product constructor.
     * @param $id
     * @param $name
     * @param $mainPhoto
     * @param $category
     * @param string $content
     */
    public function __construct($id, $name, $mainPhoto, $category, $content = '')
    {
        $this->id = $id;
        $this->name = $name;
        $this->mainPhoto = $mainPhoto;
        $this->category = $category;
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getMainPhoto()
    {
        return $this->mainPhoto;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

}