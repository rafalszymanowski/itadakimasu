<?php

namespace Raf\Itadakimasu\User;

use Doctrine\DBAL\Connection;

class RegisteredUserRepository {

    /**
     * @var Connection
     */
    private $dbh;

    const ADMIN = 'admin';

    /**
     * RegisteredUserRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param RegisteredUser $registeredUser
     * @return bool
     */
    public function loginAlreadyExists(RegisteredUser $registeredUser)
    {
        $sth = $this->dbh->prepare('SELECT `username`, `password` FROM `users` WHERE `username` = :username');
        $sth->bindValue('username', $registeredUser->getUsername(), \PDO::PARAM_STR);
        $sth->execute();
        $user = $sth->fetch();

        return ($user != false);
    }

    /**
     * @param RegisteredUser $registeredUser
     * @return bool
     */
    public function emailAlreadyExists(RegisteredUser $registeredUser)
    {
        $sth = $this->dbh->prepare('SELECT `user_email`, `password` FROM `users` WHERE `user_email` = :user_email');
        $sth->bindValue('user_email', $registeredUser->getUserEmail(), \PDO::PARAM_STR);
        $sth->execute();
        $user = $sth->fetch();
        
        return ($user != false);
    }

    /**
     * @param RegisteredUser $registeredUser
     * @return bool
     */
    public function addNewUser(RegisteredUser $registeredUser)
    {
        if($_POST['username'] !== false && strpos($_POST['user_email'], '@') !== false && isset($_POST['password']) === true)
        {
            $hashedPassword = crypt($registeredUser->getPassword(), uniqid('', true));
            $this->dbh->insert(
                    'users', [
                        'username' => $registeredUser->getUsername(),
                        'user_email' => $registeredUser->getUserEmail(),
                        'password' => $hashedPassword
                    ]);
            return true;
        }
        return false;
    }

    /**
     * @return bool|string
     */
    public function getAmountOfRegisteredUsers()
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `users`');
        $sth->execute();
        $usersAmount = $sth->fetchColumn();

        return $usersAmount - 1;
    }

    /**
     * @return array
     */
    public function getInfoAboutAdmin()
    {
        $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `username` = :username');
        $sth->bindValue('username', self::ADMIN, \PDO::PARAM_STR);
        $sth->execute();
        $adminInfo = $sth->fetch();

        return $adminInfo;
    }

    /**
     * @param $username
     * @return bool|string
     */
    public function getUserId($username)
    {
        $sth = $this->dbh->prepare('SELECT `user_id` FROM `users` WHERE `username` = :username');
        $sth->bindValue('username', $username, \PDO::PARAM_STR);
        $sth->execute();
        $userId = $sth->fetchColumn();

        return $userId;
    }
}
