<?php

namespace Raf\Itadakimasu\User;

use Doctrine\DBAL\Connection;

class UserRepository {

    /**
     * @var Connection
     */
    private $dbh;

    /**
     * UserRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function validateUser(User $user)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `username` = :username');
        $sth->bindValue('username', $user->getUsername(), \PDO::PARAM_STR);
        $sth->execute();
        $validatedUser = $sth->fetch();
        
        if(false !== $validatedUser && hash_equals($validatedUser['password'], crypt($user->getPassword(), $validatedUser['password'])))
        {
            return true;
        }
        return false;
    }

}
