<?php
/**
 * Created by PhpStorm.
 * User: Raf
 * Date: 06.05.2017
 * Time: 14:33
 */

namespace Raf\Itadakimasu\User;

class RegisteredUser
{
    /**
     * @var
     */
    private $userId;

    /**
     * @var
     */
    private $username;

    /**
     * @var
     */
    private $userEmail;

    /**
     * @var
     */
    private $password;

    /**
     * @var
     */
    private $userPhoto;

    /**
     * @var
     */
    private $content;

    /**
     * RegisteredUser constructor.
     * @param $userId
     * @param $username
     * @param $userEmail
     * @param $password
     * @param string $userPhoto
     * @param string $content
     */
    public function __construct($userId, $username, $userEmail, $password, $userPhoto = '', $content = '')
    {
        $this->userId = $userId;
        $this->username = $username;
        $this->userEmail = $userEmail;
        $this->password = $password;
        $this->userPhoto = $userPhoto;
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUserPhoto()
    {
        return $this->userPhoto;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

}