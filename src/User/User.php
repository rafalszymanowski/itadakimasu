<?php
/**
 * Created by PhpStorm.
 * User: Raf
 * Date: 06.05.2017
 * Time: 14:09
 */

namespace Raf\Itadakimasu\User;

class User
{
    /**
     * @var
     */
    private $username;

    /**
     * @var
     */
    private $password;

    /**
     * User constructor.
     * @param $username
     * @param $password
     */
    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }
}