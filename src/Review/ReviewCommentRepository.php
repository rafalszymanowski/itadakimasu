<?php
/**
 * Created by PhpStorm.
 * User: Raf
 * Date: 09.05.2017
 * Time: 20:29
 */

namespace Raf\Itadakimasu\Review;

use Doctrine\DBAL\Connection;

class ReviewCommentRepository
{
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * ReviewCommentRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param $id
     * @return array
     */
    public function getReviewComments($id)
    {
        $sth = $this->dbh->prepare(
            'SELECT
              rv.com_id,
              rv.comment,
              rv.creation_date,
              u.username
            FROM 
              `reviews_comments` AS rv 
            JOIN `reviews` AS r ON r.review_id = rv.review_id
            JOIN `users` AS u ON rv.user_id = u.user_id
            WHERE 
              r.`review_id` = :id ORDER BY `creation_date` DESC'
        );
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $reviewComment = $sth->fetchAll();

        return $reviewComment;
    }

    /**
     * @return bool|string
     */
    public function getAmountOfCommentsForReviews()
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `reviews_comments`');
        $sth->execute();
        $reviewCommentsAmount = $sth->fetchColumn();

        return $reviewCommentsAmount;
    }

    /**
     * @param $comment
     * @param $reviewId
     * @param $userId
     * @return bool|int
     */
    public function saveReviewComment($comment, $reviewId, $userId)
    {
        if(strlen($comment) > 1) {
            return $this->dbh->insert('reviews_comments', [
                'review_id' => $reviewId,
                'user_id' => $userId,
                'comment' => $comment
            ]);
        }
        return false;
    }

    /**
     * @param ReviewComment $reviewComment
     * @return int
     */
    public function deleteReviewComment(ReviewComment $reviewComment)
    {
        return $this->dbh->delete('reviews_comments', array(
            'com_id' => $reviewComment->getCommentId()
        ));
    }

}