<?php
/**
 * Created by PhpStorm.
 * UserRepository: Raf
 * Date: 05.05.2017
 * Time: 17:17
 */

namespace Raf\Itadakimasu\Review;

class Review
{
    /**
     * @var
     */
    private $id;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $city;
    /**
     * @var
     */
    private $mainPhoto;
    /**
     * @var string
     */
    private $content;

    /**
     * Review constructor.
     * @param $id
     * @param $name
     * @param $city
     * @param $mainPhoto
     * @param string $content
     */
    public function __construct($id, $name, $city, $mainPhoto, $content = '')
    {
        $this->id = $id;
        $this->name = $name;
        $this->city = $city;
        $this->mainPhoto = $mainPhoto;
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getMainPhoto()
    {
        return $this->mainPhoto;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}