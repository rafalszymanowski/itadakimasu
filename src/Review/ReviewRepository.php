<?php
/**
 * Created by PhpStorm.
 * UserRepository: Raf
 * Date: 05.05.2017
 * Time: 17:47
 */

namespace Raf\Itadakimasu\Review;

use Doctrine\DBAL\Connection;

class ReviewRepository
{
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * ReviewRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getReview($id)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `reviews` WHERE `review_id` = :id');
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $reviewContent = $sth->fetch();

        return $reviewContent;
    }

    /**
     * @return bool|string
     */
    public function getAmountOfReviews()
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `reviews`');
        $sth->execute();
        $reviewsAmount = $sth->fetchColumn();

        return $reviewsAmount;
    }

    /**
     * @param $city
     * @return array
     */
    public function getReviewsByCity($city)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `reviews` WHERE `review_city` = :review_city ORDER BY `review_add_date` DESC');
        $sth->bindValue('review_city', $city, \PDO::PARAM_STR);
        $sth->execute();
        $reviewCity = $sth->fetchAll();

        return $reviewCity;
    }

    /**
     * @param Review $review
     * @return int
     */
    public function saveReview(Review $review)
    {
        return $this->dbh->insert('reviews', [
                'review_name' => $review->getName(),
                'review_city' => $review->getCity(),
                'review_main_photo' => $review->getMainPhoto(),
                'review_content' => $review->getContent(),
            ]);
    }

    /**
     * @param Review $review
     * @return int
     */
    public function updateReview(Review $review)
    {
        return $this->dbh->update('reviews', array(
            'review_name' => $review->getName(),
            'review_city' => $review->getCity(),
            'review_main_photo' => $review->getMainPhoto(),
            'review_content' => $review->getContent()
        ), array(
            'review_id' => $review->getId()
        ));
    }

    /**
     * @param Review $review
     * @return int
     */
    public function deleteReview(Review $review)
    {
        return $this->dbh->delete('reviews', array(
            'review_id' => $review->getId()
        ));
    }

}