<?php
/**
 * Created by PhpStorm.
 * User: Raf
 * Date: 09.05.2017
 * Time: 20:27
 */

namespace Raf\Itadakimasu\Review;

class ReviewComment
{
    /**
     * @var
     */
    private $commentId;

    /**
     * @var
     */
    private $reviewId;

    /**
     * @var
     */
    private $userId;

    /**
     * @var string
     */
    private $comment;


    /**
     * ReviewComment constructor.
     * @param $commentId
     * @param $reviewId
     * @param $userId
     * @param $comment
     */
    public function __construct($commentId, $reviewId, $userId, $comment)
    {
        $this->commentId = $commentId;
        $this->reviewId = $reviewId;
        $this->userId = $userId;
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getCommentId()
    {
        return $this->commentId;
    }

    /**
     * @return mixed
     */
    public function getReviewId()
    {
        return $this->reviewId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

}