<?php
/**
 * Created by PhpStorm.
 * UserRepository: Raf
 * Date: 30.05.2017
 * Time: 19:47
 */

namespace Raf\Itadakimasu\Product;

class Product
{
    private $id;
    private $name;
    private $content;

    /**
     * Product constructor.
     * @param $id
     * @param $name
     * @param string $content
     */
    public function __construct($id, $name, $content = '')
    {
        $this->id = $id;
        $this->name = $name;
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

}