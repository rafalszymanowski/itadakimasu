<?php

namespace Raf\Itadakimasu\Product;

use Doctrine\DBAL\Connection;

class ProductRepository
{
    /**
     * @var Connection
     */
    private $dbh;

    const LIMITED_AMOUNT = 10;

    /**
     * ProductRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProduct($id)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `products` WHERE `product_id` = :id');
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $productContent = $sth->fetch();

        return $productContent;
    }

    /**
     * @param int $page
     * @return array
     */
    public function getLimitedAmountOfProductsOnList($page = 1)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `products` LIMIT :limit_from, :limit_amount');
        $sth->bindValue('limit_from', (self::LIMITED_AMOUNT*$page-self::LIMITED_AMOUNT), \PDO::PARAM_INT);
        $sth->bindValue('limit_amount', self::LIMITED_AMOUNT, \PDO::PARAM_INT);
        $sth->execute();
        $productsOnListAmount = $sth->fetchAll();

        return $productsOnListAmount;
    }

    /**
     * @return float
     */
    public function getAmountOfPages()
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `products`');
        $sth->execute();
        $pagesAmount = $sth->fetchColumn();

        return ceil($pagesAmount/self::LIMITED_AMOUNT);
    }

    /**
     * @return bool|string
     */
    public function getAmountOfProducts()
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `products`');
        $sth->execute();
        $productsAmount = $sth->fetchColumn();

        return $productsAmount;
    }

    /**
     * @param Product $product
     * @return int
     */
    public function saveProduct(Product $product)
    {
        return $this->dbh->insert('products', [
                'product_name' => $product->getName(),
                'product_content' => $product->getContent(),
            ]);
    }

    /**
     * @param $searchedString
     * @return array
     */
    public function searchForSingleProduct($searchedString)
    {
            $sth = $this->dbh->prepare('SELECT * FROM `products` WHERE `product_name` LIKE :product_name');
            $sth->bindValue('product_name', '%' . $searchedString . '%', \PDO::PARAM_STR);
            $sth->execute();
            $searchedProduct = $sth->fetchAll();

            return $searchedProduct;
    }

    /**
     * @param Product $product
     * @return int
     */
    public function updateProduct(Product $product)
    {
        return $this->dbh->update('products', array(
            'product_name' => $product->getName(),
            'product_content' => $product->getContent()
        ), array(
            'product_id' => $product->getId()
        ));
    }

    /**
     * @param Product $product
     * @return int
     */
    public function deleteProduct(Product $product)
    {
        return $this->dbh->delete('products', array(
            'product_id' => $product->getId()
        ));
    }

}