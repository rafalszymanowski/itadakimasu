<?php
session_start();

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Raf\Itadakimasu\Config\Config;
use Raf\Itadakimasu\Controller\Itadakimasu;
use Raf\Itadakimasu\Request\Request;
use Raf\Itadakimasu\Session\Session;
use Raf\Itadakimasu\View\Template;
use Raf\Itadakimasu\Recipe\RecipeRepository;
use Raf\Itadakimasu\Product\ProductRepository;
use Raf\Itadakimasu\Review\ReviewRepository;
use Raf\Itadakimasu\User\UserRepository;
use Raf\Itadakimasu\User\RegisteredUserRepository;
use Raf\Itadakimasu\Recipe\RecipeCommentRepository;
use Raf\Itadakimasu\Review\ReviewCommentRepository;

require_once __DIR__ . '/../vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(Config::TEMPLATE_DIR);
$twig = new Twig_Environment(
    $loader,
    [
        'cache' => false,
        'debug' => true
    ]
);

$twig->addExtension(new Twig_Extension_Debug());

$config = new Configuration();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$template = new Template($twig);
$request = new Request();
$session = new Session();
$recipeRepo = new RecipeRepository($dbh);
$productRepo = new ProductRepository($dbh);
$reviewRepo = new ReviewRepository($dbh);
$userRepo = new UserRepository($dbh);
$regUserRepo = new RegisteredUserRepository($dbh);
$recipeCommentRepo = new RecipeCommentRepository($dbh);
$reviewCommentRepo = new ReviewCommentRepository($dbh);

$app = new Itadakimasu(
    $template,
    $request,
    $session,
    $recipeRepo,
    $productRepo,
    $reviewRepo,
    $userRepo,
    $regUserRepo,
    $recipeCommentRepo,
    $reviewCommentRepo,
    $dbh
);

$app->run();