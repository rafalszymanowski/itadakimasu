$(document).ready(function(){

    $('#myCarousel').find('.carousel-inner div.item:first-child').addClass('active');

    $('.carousel').carousel({
        pause: null
    });

    $('#log-submit').click(function () {

        var form = $('#log-form');

        $.ajax({
            type:"POST",
            cache:false,
            url:form.attr('action'),
            data:form.serialize(),
            dataType: 'json',
            success: function (json_data) {
                if(json_data.data === 'ok') {
                    window.location.href = "index.php?action=homepage";
                } else {
                    $('#log-errors').removeClass('hidden').text(json_data.errors);
                }
            }
        });
        return false;
    });

    $('#reg-submit').click(function () {

        var form = $('#reg-form');

        $.ajax({
            type:"POST",
            cache:false,
            url:form.attr('action'),
            data:form.serialize(),
            dataType: 'json',
            success: function (json_data) {
                if(json_data.data === 'ok') {
                    window.location.href = "index.php?action=homepage";
                } else {
                    $('#reg-errors').removeClass('hidden').text(json_data.errors);
                }
            }
        });
        return false;
    });

    $('.back-to-top').click(function() {
        $('body').animate({scrollTop: 0}, 800);
    });

});
